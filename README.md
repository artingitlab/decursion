# Decursion: recursion detector

Decursion is decorators at callable and class levels to detect recursion (currently limited only for function, method, static method, and property.)
If you're satisfied with python's built-in recursion check counter, then you do not have no use of this. 


## Spec
  - Decorator at class level.
    - Allow custom condition for which callable to be monitored for recursion detection. 
  - Decorator at callable level - function, method, static method, and property.
    - Allow custom condition to skip detection.

## Remark
  - Please be advised that this does not automatically monitor any subsequent 
    call from a decorated/wrapped callable unless the another callable being 
    called by such subsequent call is also decorated/wrapped.

## Functions
- ### `detect_recursion`

    #### Description

    A decorator function for detecting recursion in other functions.

    #### Usage

    ```python
    def shall_bypass(*args, **kwargs) -> bool:
        if args and args[0] < 2:
            return True  # To bypass recursion check
        return False  # Not to bypass recursion check    
  
    @detect_recursion(shall_bypass=shall_bypass)
    def factorial(n):
        if n == 1:
            return 1
        return n * factorial(n-1)
    ```

    #### Parameters

    - `shall_bypass`: A callback function to bypass the recursion detection for certain conditions.

    ---

- ### `detect_recursion_at_class`

    #### Description

    A decorator function for detecting recursion in methods and properties at the class level.

    #### Usage

    ```python
        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == 'factorial':
                def shall_bypass(*args, **kwargs) -> bool:
                    if args and args[1] < 2:
                        return True     # To bypass recursion check
                    return False        # Not to bypass recursion check
                return True, shall_bypass  # To wrap factorial method.
            return False, None  # Not to wrap other than factorial method.
    
    @detect_recursion_at_class(shall_wrap=shall_wrap)
    class MyClass:
        def factorial(self, n):
            if n == 1:
                return 1
            return n * self.factorial(n-1)
    ```

    #### Parameters

    - `shall_wrap`: A callback function to determine whether to wrap a method or property for recursion detection.

    ---