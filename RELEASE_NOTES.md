
1.1.3 / 2024-09-29
==================

  * Fix of callable subject in static method case

1.1.2 / 2024-09-24
==================

  * Fix to handle static method call on instance instead of class
