#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from icecream import ic
import inspect
from functools import wraps
import threading
from typing import Any, Callable, Optional, Tuple, Type, cast


def detect_recursion(shall_bypass: Optional[Callable[[Any, Any], bool]] = None) -> Callable:
    """
    Decorator function for detecting recursion in other functions.

    Parameters:
    -----------
    shall_bypass : Optional[Callable[[Any, Any], bool]], optional
        A callback function that can be used to bypass the recursion detection for certain conditions.
        It should take two arguments: the first for positional arguments and the second for keyword arguments
        of the function being decorated. Default is None.

    Returns:
    --------
    Callable
        A decorator that can be applied to other functions for recursion detection.

    Example:
    --------
    @detect_recursion
    def factorial(n):
        if n == 1:
            return 1
        return n * factorial(n-1)
    """

    thread_local = threading.local()

    def recursion_detection_decorator(func: Callable):
        if isinstance(func, staticmethod):
            # special wrapper for static method due to necessity of removing first
            # positional argument (self object) for case of executing static method
            # on instance object rather than class object.
            underlying_func = ic(func.__func__)
            unwrapped_func = inspect.unwrap(underlying_func)

            @wraps(underlying_func)
            def recursion_detection_wrapper(*args, **kwargs):
                ic(args, kwargs)

                if len(args) > 0:
                    func_mbr = ic(getattr(args[0], underlying_func.__name__, None))
                    is_same_func = False
                    if inspect.isfunction(func_mbr) or inspect.ismethod(func_mbr):
                        is_same_func = ic(inspect.unwrap(func_mbr) == unwrapped_func)

                    if is_same_func:
                        try:
                            callargs = ic(inspect.getcallargs(underlying_func, *args, **kwargs))
                        except TypeError as e:
                            ic("TypeError in integrity check on arguments to {0} before actual call: {1}".format(
                                underlying_func, e))
                            args = ic(args[1:])  # Remove the first argument and try again
                            try:
                                callargs = ic(inspect.getcallargs(underlying_func, *args, **kwargs))
                            except TypeError as e2:
                                ic("TypeError in integrity check on arguments to {0} before actual call: {1}".format(
                                    underlying_func, e2))
                                raise

                if shall_bypass and shall_bypass(*args, **kwargs):
                    return func(*args, **kwargs)

                # Initialize call count if not set
                if not hasattr(thread_local, 'call_count'):
                    thread_local.call_count = 0

                # Increment call count
                thread_local.call_count += 1

                # Check for excessive recursion
                if thread_local.call_count > 1:
                    thread_local.call_count = 0  # Re   set count before raising
                    err_msg = f"Infinite recursion detected in {func.__name__}" if not isinstance(func, property) \
                        else f"Infinite recursion detected in a property of {type(args[0]).__name__} class"
                    raise RecursionError(err_msg)

                result = None
                try:
                    result = func(*args, **kwargs)
                finally:
                    # Always reset call_count to 0
                    thread_local.call_count = 0

                return result

            return recursion_detection_wrapper

        else:
            @wraps(wrapped=func)
            def recursion_detection_wrapper(*args, **kwargs):
                if shall_bypass and shall_bypass(*args, **kwargs):
                    if isinstance(func, property):
                        return cast(property, func).fget(*args)
                    else:
                        return func(*args, **kwargs)

                # Initialize call count if not set
                if not hasattr(thread_local, 'call_count'):
                    thread_local.call_count = 0

                # Increment call count
                thread_local.call_count += 1

                # Check for excessive recursion
                if thread_local.call_count > 1:
                    thread_local.call_count = 0  # Re   set count before raising
                    err_msg = f"Infinite recursion detected in {func.__name__}" if not isinstance(func, property) \
                        else f"Infinite recursion detected in a property of {type(args[0]).__name__} class"
                    raise RecursionError(err_msg)

                result = None
                try:
                    if isinstance(func, property):
                        result = cast(property, func).fget(*args)
                    else:
                        result = func(*args, **kwargs)
                finally:
                    # Always reset call_count to 0
                    thread_local.call_count = 0

                return result
            return recursion_detection_wrapper

    return recursion_detection_decorator


def detect_recursion_at_class(
        shall_wrap: Callable[[Type, str], Tuple[bool, Optional[Callable[[Any, Any], bool]]]]) \
        -> Callable[[Type], Type]:
    """
    Decorator at the class level to detect recursion at callable in class.

    Parameters:
    -----------
    shall_wrap : Callable[[Type, str], Tuple[bool, Optional[Callable[[Any, Any], bool]]]]
        A callback function that determines whether a method or property of a class should be wrapped
        for recursion detection. It should take two arguments: the first is the type of the class and
        the second is the name of the callable (method or property). It should return a tuple containing
        a boolean to indicate whether to wrap the callable, and optionally a function that can be used
        to bypass the recursion detection.

    Returns:
    --------
    Callable[[Type], Type]
        A decorator that can be applied to classes for recursion detection at the class level.

    Example:
    --------
    @detect_recursion_at_class
    class MyClass:
        def my_method(self):
            pass
    """

    def recursion_detection_decorator_at_class(cls: type):
        mbs = dict(inspect.getmembers(cls))
        # screen to only callable and property
        callable_mbs = dict(filter(lambda t: callable(t[1]) or isinstance(t[1], property), mbs.items()))

        if shall_wrap:
            for name, callable_obj in callable_mbs.items():
                wrap_condition, shall_bypass_func = shall_wrap(cls, name)
                if not wrap_condition:
                    continue

                wrapped = detect_recursion(shall_bypass=shall_bypass_func)(callable_obj)
                if isinstance(callable_obj, property):
                    setattr(cls, name, property(wrapped))
                else:
                    setattr(cls, name, wrapped)
        return cls
    return recursion_detection_decorator_at_class
