#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
from decursion.Decursion import detect_recursion, detect_recursion_at_class
import functools
from icecream import ic
import inspect
import logging
import math
import pytest
import tempfile
import threading
from typing import Callable, Dict, Optional, Tuple, cast

import atexit
import pdb


wrapper_call_count = 0


def shall_bypass(*args, **kwargs) -> bool:
    global wrapper_call_count
    wrapper_call_count += 1
    if args and len(args) == 1 and args[0] < 2:
        return True  # To bypass recursion check
    return False  # Not to bypass recursion check


@detect_recursion(shall_bypass=shall_bypass)
def factorial(n):
    if n == 1:
        return 1
    return n * factorial(n-1)


def test_recursion_function():
    global wrapper_call_count;
    assert wrapper_call_count == 0, \
        ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
         "value {0} set before actual checking.".format(wrapper_call_count))

    assert factorial(2) == 2
    expected_counted_call = 2
    assert wrapper_call_count == expected_counted_call, \
        ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
            expected_counted_call, wrapper_call_count))

    wrapper_call_count = 0
    with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(factorial.__name__)):
        factorial(3)
    expected_counted_call = 2
    assert wrapper_call_count == expected_counted_call, \
        ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
            expected_counted_call, wrapper_call_count))

    wrapper_call_count = 0


class TestDetectRecursionAtClass:
    def test_regular_method(self):  # case of decorating regular method (not with recursion logic)
        method_name = 'regular_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                return True, None   # To wrap regular_method method.
            return False, None  # Not to wrap other than regular_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def regular_method(self):
                return Dummy.regular_method.__name__

        assert method_name == Dummy.regular_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.regular_method.__name__, method_name)

        subject_obj = Dummy()
        assert subject_obj.regular_method() == subject_obj.regular_method.__name__

    def test_regular_method_not_wrapped(self):  # a case of not-decorating regular method (not with recursion logic)
        method_name = 'regular_method_not_wrapped'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            return False, None  # Not to wrap any method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def regular_method_not_wrapped(self):
                return Dummy.regular_method_not_wrapped.__name__

        assert method_name == Dummy.regular_method_not_wrapped.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.regular_method_not_wrapped.__name__, method_name)

        subject_obj = Dummy()
        assert subject_obj.regular_method_not_wrapped() == subject_obj.regular_method_not_wrapped.__name__

    def test_recursion_method(self):    # case of decorating regular method with recursion logic
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_recursion_method(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    if args and len(args) == 2 and args[1] < 2:
                        return True     # To bypass recursion check
                    return False        # Not to bypass recursion check
                return True, shall_bypass_recursion_method  # To wrap recursion_method method.
            return False, None  # Not to wrap other than recursion_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        assert method_name == Dummy.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = Dummy()
        assert subject_obj.recursion_method(2) == 3
        expected_counted_call = 3
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_call_count = 0
        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            subject_obj.recursion_method(3)
        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_recursion_method_not_wrapped(self):    # a case of not-decorating regular method with recursion logic
        method_name = 'recursion_method_not_wrapped'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            return False, None  # Not to wrap any method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def recursion_method_not_wrapped(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method_not_wrapped(x - 1)

        assert method_name == Dummy.recursion_method_not_wrapped.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.recursion_method_not_wrapped.__name__, method_name)

        subject_obj = Dummy()
        assert subject_obj.recursion_method_not_wrapped(3) == 6

    def test_subsequence_among_wrapped_methods(self):  # case of cascade call of decorated regular methods
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method3_name = 'regular_method3'
        method2_name = 'regular_method2'
        method1_name = 'regular_method1'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name in [method3_name, method2_name, method1_name]:
                def shall_bypass_regular_methods_cascade(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_regular_methods_cascade
            return False, None

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def regular_method1(self):
                return Dummy.regular_method1.__name__

            def regular_method2(self):
                return self.regular_method1()

            def regular_method3(self):
                return self.regular_method2()

        assert method3_name == Dummy.regular_method3.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method3_name', Dummy.regular_method3.__name__, method3_name)
        assert method2_name == Dummy.regular_method2.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method2_name', Dummy.regular_method2.__name__, method2_name)
        assert method1_name == Dummy.regular_method1.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method1_name', Dummy.regular_method1.__name__, method1_name)

        subject_obj = Dummy()
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert subject_obj.regular_method3() == subject_obj.regular_method1.__name__
        expected_counted_call = 3
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_subsequence_from_unwrapped_method_to_wrapped_recursion_method(self):
        # a case of cascade call to decorated recursion method from undecorated regular method

        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_recursion_method(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_recursion_method  # To wrap recursion_method method.
            return False, None  # Not to wrap other than recursion_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def regular_method(self, x):
                return self.recursion_method(x=x)

            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        assert method_name == Dummy.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.recursion_method.__name__, method_name)

        subject_obj = Dummy()
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            subject_obj.regular_method(x=3)
        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_subsequence_from_wrapped_method_to_unwrapped_recursion_method(self):
        # a case of cascade call of decorated regular methods
        # Memo: Recursion detection monitors only on call to decorated/wrapped callable but not on subsequent call
        # to undecorated/non-wrapped callable even from decorated/wrapped one. And this is a test case for
        # such subsequent call.

        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'regular_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_regular_method(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_regular_method    # To wrap regular_method method.
            return False, None  # Not to wrap other than regular_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def regular_method(self, x):
                return self.recursion_method(x=x)

            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        assert method_name == Dummy.regular_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.regular_method.__name__, method_name)

        subject_obj = Dummy()
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert 6 == subject_obj.regular_method(x=3)
        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_subsequence_from_unwrapped_method_to_unwrapped_recursion_method(self):
        # a case of cascade call to undecorated recursion method from undecorated regular method

        method_name = 'regular_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            return False, None  # Not to wrap any method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def regular_method(self, x):
                return self.recursion_method(x=x)

            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        assert method_name == Dummy.regular_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.regular_method.__name__, method_name)

        subject_obj = Dummy()
        assert 6 == subject_obj.regular_method(x=3)

    def test_static_method(self, caplog):  # a case of decorating static method (not with recursion logic)
        thread_local = threading.local()
        thread_local.has_wrapper_called = False

        method_name = 'static_method'
        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_recursion_static_method(*args, **kwargs) -> bool:
                    thread_local.has_wrapper_called = True
                    return False    # Not to bypass recursion check
                return True, shall_bypass_recursion_static_method   # To wrap static_method method.
            return False, None  # Not to wrap other than static_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @staticmethod
            def static_method():
                return Dummy.static_method.__name__

        assert method_name == Dummy.static_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.static_method.__name__, method_name)

        assert not thread_local.has_wrapper_called, \
            ("ERROR in TEST: thread-local variable being used to check whether wrapper is "
             "called already got True set before actual checking.")
        assert Dummy.static_method() == Dummy.static_method.__name__
        assert thread_local.has_wrapper_called, \
            "Expected wrapper of {0} static method had called but actually not.".format(Dummy.static_method.__name__)

        thread_local = None

    def test_static_method_not_wrapped(self, caplog):  # a case of decorating static method (not with recursion logic)
        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            return False, None  # Not to wrap any method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @staticmethod
            def static_method_not_wrapped():
                return Dummy.static_method_not_wrapped.__name__

        assert Dummy.static_method_not_wrapped() == Dummy.static_method_not_wrapped.__name__

    def test_recursion_static_method(self): # a case of decorating static method with recursion logic
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_static_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_recursion_static_method(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    if args and len(args) == 1 and args[0] < 2:
                        return True     # To bypass recursion check
                    return False        # Not to bypass recursion check
                return True, shall_bypass_recursion_static_method   # To wrap recursion_static_method method.
            return False, None  # Not to wrap other than recursion_static_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @staticmethod
            def recursion_static_method(x):
                if x <= 0:
                    return 0
                return x + Dummy.recursion_static_method(x - 1)

        assert method_name == Dummy.recursion_static_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.recursion_static_method.__name__, method_name)

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        assert Dummy.recursion_static_method(2) == 3
        expected_counted_call = 3
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_call_count = 0
        with pytest.raises(
                RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            Dummy.recursion_static_method(3)
        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_class_method(self):  # a case of decorating regular class method
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'regular_class_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_callable    # To wrap regular_class_method class method.
            return False, None  # Not to wrap other than regular_class_method class method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            _class_method_value = "class method value"

            @classmethod
            def regular_class_method(cls):
                return cls._class_method_value

        assert method_name == Dummy.regular_class_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.regular_class_method.__name__, method_name)

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert Dummy._class_method_value == Dummy.regular_class_method()
        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_recursion_class_method(self):  # a case of decorating recursive class method
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_class_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_callable    # To wrap recursion_class_method class method.
            return False, None  # Not to wrap other than recursion_class_method class method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @classmethod
            def recursion_class_method(cls):
                return cls.recursion_class_method()

        assert method_name == Dummy.recursion_class_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.recursion_class_method.__name__, method_name)

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        with pytest.raises(RecursionError,
                           match="Infinite recursion detected in {0}".format(method_name)):
            Dummy.recursion_class_method()

        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_recursive_property(self):  # a case of decorating property with recursion reference
        property_name = 'recursive_property'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == property_name:
                def shall_bypass_recursive_property(*args, **kwargs) -> bool:
                    return False    # Not to bypass recursion check
                return True, shall_bypass_recursive_property    # To wrap recursive_property property.
            return False, None  # Not to wrap other than recursive_property property.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @property
            def recursive_property(self):
                return self.recursive_property

        mbr_dict = dict(inspect.getmembers(Dummy, predicate=(lambda x: isinstance(x, property))))
        assert len(mbr_dict) == 1 and property_name == list(mbr_dict.keys())[0], \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'property_name', list(mbr_dict.keys())[0], property_name)

        subject_obj = Dummy()
        with pytest.raises(RecursionError,
                           match="Infinite recursion detected in a property of {0}".format(type(subject_obj).__name__)):
            prop_value = subject_obj.recursive_property

    def test_subsequence_among_wrapped_property(self):  # case of cascade reference of decorated properties.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property1_name = 'regular_property1'
        property2_name = 'regular_property2'
        property3_name = 'regular_property3'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name in [property1_name, property2_name, property3_name]:
                def shall_bypass_regular_properties_cascade(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_regular_properties_cascade    # To wrap those properties.
            return False, None  # Not to wrap other than regular_property1, .regular_property2, and regular_property3

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @property
            def regular_property1(self):
                return property1_name

            @property
            def regular_property2(self):
                return self.regular_property1

            @property
            def regular_property3(self):
                return self.regular_property2

        mbr_dict = dict(inspect.getmembers(Dummy, predicate=(lambda x: isinstance(x, property))))
        assert len(mbr_dict) == 3 and [property1_name, property2_name, property3_name] == sorted(mbr_dict.keys()), \
            ("ERROR in TEST: detected unknown/unexpected property name(s). Property names should have been set to "
             "{0} but actual {1}.").format([property1_name, property2_name, property3_name],
                                           sorted(mbr_dict.keys()))

        subject_obj = Dummy()
        assert subject_obj.regular_property3 == property1_name
        expected_counted_call = 3
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_subsequence_from_unwrapped_property_to_wrapped_recursion_property(self):
        # a case of cascade call to decorated recursion property from undecorated regular property

        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'recursion_property'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == property_name:
                def shall_bypass_recursion_property(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False  # Not to bypass recursion check
                return True, shall_bypass_recursion_property  # To wrap recursion_property property.
            return False, None  # Not to wrap other than recursion_property property.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @property
            def regular_property(self):
                return self.recursion_property

            @property
            def recursion_property(self):
                return self.recursion_property

        mbr_dict = dict(inspect.getmembers(Dummy, predicate=(lambda x: isinstance(x, property))))
        assert len(mbr_dict) == 2 and property_name in list(mbr_dict.keys()), \
            ("ERROR in TEST: value of {0} variable needs to set name of existing property, "
             "but {1} is not in existing property names {2}.").format(
                'property_name', property_name, list(mbr_dict.keys())[0])

        subject_obj = Dummy()
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        with pytest.raises(RecursionError,
                           match="Infinite recursion detected in a property of {0}".format(type(subject_obj).__name__)):
            regular_property_value = subject_obj.regular_property
        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_inherit_recursion_method(self):    # case of calling parent's decorated recursion method.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    if args and len(args) == 2 and args[1] < 2:
                        return True     # To bypass recursion check
                    return False    # Not to bypass recursion check
                return True, shall_bypass_callable  # To wrap recursion_method method.
            return False, None  # Not to wrap other than recursion_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        class DummyChild(Dummy):
            def regular_method(self, x):
                return self.recursion_method(x=x)

        assert method_name == DummyChild.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', DummyChild.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = DummyChild()
        assert subject_obj.regular_method(2) == 3   # Call DemoChild.regular_method
        expected_counted_call = 3
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_call_count = 0
        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            subject_obj.regular_method(3)   # Call DemoChild.regular_method
        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overridden_recursion_method(self):    # case of calling overriden recursion method.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    if args and len(args) == 2 and args[1] < 2:
                        return True     # To bypass recursion check
                    return False    # Not to bypass recursion check
                return True, shall_bypass_callable  # To wrap recursion_method method.
            return False, None  # Not to wrap other than recursion_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        class DemoChild(Dummy):
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return math.pow(x, 2) + self.recursion_method(x - 1)

        assert method_name == DemoChild.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', DemoChild.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = DemoChild()
        assert subject_obj.recursion_method(3) == 14   # Call DemoChild.recursion_method
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overriding_recursion_method(self):    # case of calling overriding recursion method.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    if args and len(args) == 2 and args[1] < 2:
                        return True     # To bypass recursion check
                    return False    # Not to bypass recursion check
                return True, shall_bypass_callable  # To wrap recursion_method method.
            return False, None  # Not to wrap other than recursion_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        thread_local.wrapper_child_call_count = 0

        def shall_wrap_child(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_child_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_child_call_count += 1
                    if args and len(args) == 2 and args[1] < 2:
                        return True     # To bypass recursion check
                    return False    # Not to bypass recursion check
                return True, shall_bypass_child_callable  # To wrap recursion_method method.
            return False, None  # Not to wrap other than recursion_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap_child)
        class DummyChild(Dummy):
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return math.pow(x, 2) + self.recursion_method(x - 1)

        assert method_name == DummyChild.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', DummyChild.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert thread_local.wrapper_child_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_child_call_count))

        subject_obj = DummyChild()
        assert subject_obj.recursion_method(2) == 5
        expected_child_counted_call = 3
        assert thread_local.wrapper_child_call_count == expected_child_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_child_counted_call, thread_local.wrapper_child_call_count))
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_child_call_count = 0
        thread_local.wrapper_call_count = 0

        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            subject_obj.recursion_method(3)
        expected_child_counted_call = 2
        assert thread_local.wrapper_child_call_count == expected_child_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_child_counted_call, thread_local.wrapper_child_call_count))
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overriding_methodlevel_decoration(self):
        # case of overriding method-level decoration by class-level decoration
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_bypass_callable(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            if args and len(args) == 2 and args[1] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass_callable)
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        thread_local.wrapper_child_call_count = 0

        def shall_wrap_child(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == method_name:
                def shall_bypass_child_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_child_call_count += 1
                    if args and len(args) == 2 and args[1] < 2:
                        return True     # To bypass recursion check
                    return False    # Not to bypass recursion check
                return True, shall_bypass_child_callable  # To wrap recursion_method method.
            return False, None  # Not to wrap other than recursion_method method.

        @detect_recursion_at_class(shall_wrap=shall_wrap_child)
        class DummyChild(Dummy):
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return math.pow(x, 2) + self.recursion_method(x - 1)

        assert method_name == DummyChild.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', DummyChild.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert thread_local.wrapper_child_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_child_call_count))

        subject_obj = DummyChild()
        assert subject_obj.recursion_method(2) == 5
        expected_child_counted_call = 3
        assert thread_local.wrapper_child_call_count == expected_child_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_child_counted_call, thread_local.wrapper_child_call_count))
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_child_call_count = 0
        thread_local.wrapper_call_count = 0

        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            subject_obj.recursion_method(3)
        expected_child_counted_call = 2
        assert thread_local.wrapper_child_call_count == expected_child_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_child_counted_call, thread_local.wrapper_child_call_count))
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overridden_recursion_property(self):   # case of calling overriden recursion property
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'recursion_property'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == property_name:
                def shall_bypass_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_callable  # To wrap recursion_property property.
            return False, None  # Not to wrap other than recursion_property property.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @property
            def recursion_property(self):
                return self.recursion_property

        class DemoChild(Dummy):
            @property
            def recursion_property(self):
                return self.recursion_property

        mbr_dict = dict(inspect.getmembers(DemoChild, predicate=(lambda x: isinstance(x, property))))
        assert len(mbr_dict) == 1 and property_name == list(mbr_dict.keys())[0], \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'property_name', list(mbr_dict.keys())[0], property_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = DemoChild()
        with pytest.raises(RecursionError, match="maximum recursion depth exceeded"):
            prop_value = subject_obj.recursion_property
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overriding_recursion_property(self):   # case of calling overriding recursion property.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'recursion_property'

        def shall_wrap(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == property_name:
                def shall_bypass_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_callable  # To wrap recursion_property property.
            return False, None  # Not to wrap other than recursion_property property.

        @detect_recursion_at_class(shall_wrap=shall_wrap)
        class Dummy:
            @property
            def recursion_property(self):
                return self.recursion_property

        thread_local.wrapper_child_call_count = 0

        def shall_wrap_child(cls: type, callable_name: str) -> Tuple[bool, Optional[Callable]]:
            if callable_name == property_name:
                def shall_bypass_child_callable(*args, **kwargs) -> bool:
                    thread_local.wrapper_child_call_count += 1
                    return False    # Not to bypass recursion check
                return True, shall_bypass_child_callable  # To wrap recursion_property property.
            return False, None  # Not to wrap other than recursion_property property.

        @detect_recursion_at_class(shall_wrap=shall_wrap_child)
        class DummyChild(Dummy):
            @property
            def recursion_property(self):
                return self.recursion_property

        mbr_dict = dict(inspect.getmembers(DummyChild, predicate=(lambda x: isinstance(x, property))))
        assert len(mbr_dict) == 1 and property_name == list(mbr_dict.keys())[0], \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'property_name', list(mbr_dict.keys())[0], property_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert thread_local.wrapper_child_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_child_call_count))

        subject_obj = DummyChild()
        with pytest.raises(RecursionError,
                           match="Infinite recursion detected in a property of {0}".format(type(subject_obj).__name__)):
            prop_value = subject_obj.recursion_property
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))
        expected_counted_child_call = 2
        assert thread_local.wrapper_child_call_count == expected_counted_child_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_child_call, thread_local.wrapper_child_call_count))

        thread_local = None


class TestDetectRecursion:
    @pytest.fixture(scope='class', autouse=True)
    def logger_fixture(self) -> logging.Logger:
        logger = logging.getLogger(self.__class__.__name__)
        logger.setLevel(logging.DEBUG)

        # create file handler which logs even debug messages
        tempf_fd, tempf_name = tempfile.mkstemp(prefix=("{0}.".format(logger.name)),
                                                suffix=".log")
        fh = logging.FileHandler(tempf_name)
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.ERROR)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # add the handlers to logger
        logger.addHandler(ch)
        logger.addHandler(fh)

        ic.configureOutput(includeContext=True,
                           outputFunction=(lambda s: logger.debug(s)))
        self.logger = logger

        ic.enable()
        yield logger
        ic.disable()

    def test_regular_method(self):  # case of decorating regular method (not with recursion logic)
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass)
            def regular_method(self):
                return self.regular_method.__name__

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = Dummy()
        assert subject_obj.regular_method() == Dummy.regular_method.__name__

        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_recursion_method(self):    # case of decorating recursion method
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            if args and len(args) == 2 and args[1] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass)
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        assert method_name == Dummy.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = Dummy()
        assert subject_obj.recursion_method(2) == 3
        expected_counted_call = 3
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_call_count = 0
        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            subject_obj.recursion_method(3)
        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_inherit_recursion_method(self):    # case of calling parent's decorated recursion method.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            if args and len(args) == 2 and args[1] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass)
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        class DummyChild(Dummy):
            def regular_method(self, x):
                return self.recursion_method(x=x)

        assert method_name == Dummy.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = DummyChild()
        assert subject_obj.regular_method(2) == 3
        expected_counted_call = 3
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_call_count = 0
        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            subject_obj.regular_method(3)
        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overridden_recursion_method(self):    # case of calling overriden recursion method.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_bypass_callable(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            if args and len(args) == 2 and args[1] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass_callable)
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        class DemoChild(Dummy):
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return math.pow(x, 2) + self.recursion_method(x - 1)

        assert method_name == DemoChild.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', DemoChild.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = DemoChild()
        assert subject_obj.recursion_method(3) == 14   # Call DemoChild.recursion_method
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overriding_recursion_method(self):    # case of calling overriding recursion method.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_method'

        def shall_bypass_callable(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            if args and len(args) == 2 and args[1] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass_callable)
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return x + self.recursion_method(x - 1)

        thread_local.wrapper_child_call_count = 0

        def shall_bypass_child_callable(*args, **kwargs) -> bool:
            thread_local.wrapper_child_call_count += 1
            if args and len(args) == 2 and args[1] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class DummyChild(Dummy):
            @detect_recursion(shall_bypass=shall_bypass_child_callable)
            def recursion_method(self, x):
                if x <= 0:
                    return 0
                return math.pow(x, 2) + self.recursion_method(x - 1)

        assert method_name == DummyChild.recursion_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', DummyChild.recursion_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert thread_local.wrapper_child_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_child_call_count))

        subject_obj = DummyChild()
        assert subject_obj.recursion_method(2) == 5
        expected_child_counted_call = 3
        assert thread_local.wrapper_child_call_count == expected_child_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_child_counted_call, thread_local.wrapper_child_call_count))
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_child_call_count = 0
        thread_local.wrapper_call_count = 0

        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            subject_obj.recursion_method(3)
        expected_child_counted_call = 2
        assert thread_local.wrapper_child_call_count == expected_child_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_child_counted_call, thread_local.wrapper_child_call_count))
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_regular_static_method(self):  # case of decorating static regular method (not with recursion logic)
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def ws_regular_static_method(x, y=1) -> Dict:
                return {'x': x, 'y': y}

            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass)
            def sw_regular_static_method(x, y=1) -> Dict:
                return {'x': x, 'y': y}

            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def ws_is_Dummy(obj) -> bool:
                return True if isinstance(obj, type) and obj.__name__ == Dummy.__name__ else isinstance(obj, Dummy)

            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass)
            def sw_is_Dummy(obj) -> bool:
                return True if isinstance(obj, type) and obj.__name__ == Dummy.__name__ else isinstance(obj, Dummy)

            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def ws_single_kwarg_static_method(param: bool=False) -> bool:
                return param

            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass)
            def sw_single_kwarg_static_method(param: bool=False) -> bool:
                return param

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        params = {'x': 1, 'y': 2}
        assert Dummy.ws_regular_static_method(1, y=params['y']) == params
        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        params = {'x': 'l', 'y': 1}
        assert Dummy.sw_regular_static_method(params['x']) == params
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert Dummy.ws_is_Dummy(self) is False
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert Dummy.sw_is_Dummy(self) is False
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert Dummy.ws_single_kwarg_static_method(param=True) is True
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert Dummy.sw_single_kwarg_static_method(param=True) is True
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    # case of regular static method (not with recursion logic) from instance
    def test_regular_static_method_from_instance(self, caplog, logger_fixture: logging.Logger):
        caplog.set_level(level=logging.DEBUG)

        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def ws_regular_static_method(x, y=1) -> Dict:
                return {'x': x, 'y': y}

            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass)
            def sw_regular_static_method(x, y=1) -> Dict:
                return {'x': x, 'y': y}

            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def ws_is_Dummy(obj) -> bool:
                return True if isinstance(obj, type) and obj.__name__ == Dummy.__name__ else isinstance(obj, Dummy)

            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass)
            def sw_is_Dummy(obj) -> bool:
                return True if isinstance(obj, type) and obj.__name__ == Dummy.__name__ else isinstance(obj, Dummy)

            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def ws_single_kwarg_static_method(param: bool=False) -> bool:
                return param

            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass)
            def sw_single_kwarg_static_method(param: bool=False) -> bool:
                return param

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        dummy = Dummy()

        params = {'x': 1, 'y': 2}
        assert dummy.ws_regular_static_method(1, y=params['y']) == params
        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        params = {'x': 'l', 'y': 1}
        assert dummy.sw_regular_static_method(params['x']) == params
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert dummy.ws_is_Dummy(self) is False
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert dummy.sw_is_Dummy(self) is False
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert dummy.ws_single_kwarg_static_method(param=True) is True
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert dummy.sw_single_kwarg_static_method(param=True) is True
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_parent_static_method(self):
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'recursion_property'

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def ws_is_Dummy(obj) -> bool:
                return True if isinstance(obj, type) and obj.__name__ == Dummy.__name__ else isinstance(obj, Dummy)

            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass)
            def sw_is_Dummy(obj) -> bool:
                return True if isinstance(obj, type) and obj.__name__ == Dummy.__name__ else isinstance(obj, Dummy)

        class DummyChild(Dummy):
            pass

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        assert DummyChild.ws_is_Dummy(self) is False
        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert DummyChild.sw_is_Dummy(self) is False
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

    #TODO: add tests for case of static method call of parent class from inherited class/instance.
    def test_parent_static_method_from_instance(self):
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'recursion_property'

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def ws_is_Dummy(obj) -> bool:
                return True if isinstance(obj, type) and obj.__name__ == Dummy.__name__ else isinstance(obj, Dummy)

            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass)
            def sw_is_Dummy(obj) -> bool:
                return True if isinstance(obj, type) and obj.__name__ == Dummy.__name__ else isinstance(obj, Dummy)

        class DummyChild(Dummy):
            pass

        dummy_child = DummyChild()

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        assert dummy_child.ws_is_Dummy(self) is False
        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        assert dummy_child.sw_is_Dummy(self) is False
        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

    def test_static_recursion_method(self):  # case of decorating static recursion method
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_static_method'

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            if args and len(args) == 1 and args[0] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class Dummy:
            @detect_recursion(shall_bypass=shall_bypass)
            @staticmethod
            def recursion_static_method(x):
                if x <= 0:
                    return 0
                return x + Dummy.recursion_static_method(x - 1)

        assert method_name == Dummy.recursion_static_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', Dummy.recursion_static_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        assert Dummy.recursion_static_method(2) == 3
        expected_counted_call = 3
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_call_count = 0
        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            Dummy.recursion_static_method(3)
        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overriding_static_recursion_method(self):    # case of calling overriding static recursion method.
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        method_name = 'recursion_static_method'

        def shall_bypass_callable(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            if args and len(args) == 1 and args[0] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class Dummy:
            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass_callable)
            def recursion_static_method(x):
                if x <= 0:
                    return 0
                return x + Dummy.recursion_static_method(x - 1)

        thread_local.wrapper_child_call_count = 0

        def shall_bypass_child_callable(*args, **kwargs) -> bool:
            thread_local.wrapper_child_call_count += 1
            if args and len(args) == 1 and args[0] < 2:
                return True  # To bypass recursion check
            return False  # Not to bypass recursion check

        class DummyChild(Dummy):
            @staticmethod
            @detect_recursion(shall_bypass=shall_bypass_child_callable)
            def recursion_static_method(x):
                if x <= 0:
                    return 0
                return math.pow(x, 2) + DummyChild.recursion_static_method(x - 1)

        assert method_name == DummyChild.recursion_static_method.__name__, \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'method_name', DummyChild.recursion_static_method.__name__, method_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert thread_local.wrapper_child_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_child_call_count))

        assert DummyChild.recursion_static_method(2) == 5
        expected_child_counted_call = 3
        assert thread_local.wrapper_child_call_count == expected_child_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_child_counted_call, thread_local.wrapper_child_call_count))
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local.wrapper_child_call_count = 0
        thread_local.wrapper_call_count = 0

        with pytest.raises(RecursionError, match="Infinite recursion detected in {0}".format(method_name)):
            DummyChild.recursion_static_method(3)
        expected_child_counted_call = 2
        assert thread_local.wrapper_child_call_count == expected_child_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_child_counted_call, thread_local.wrapper_child_call_count))
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_regular_property(self):  # case of decorating regular property (not with recursion logic)
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'regular_property'

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @property
            @detect_recursion(shall_bypass=shall_bypass)
            def regular_property(self):
                return property_name

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        subject_obj = Dummy()
        assert subject_obj.regular_property == property_name

        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_recursion_property(self):  # case of decorating recursion property
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'recursion_property'

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @property
            @detect_recursion(shall_bypass=shall_bypass)
            def recursion_property(self):
                return self.recursion_property

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        mbr_dict = dict(inspect.getmembers(Dummy, predicate=(lambda x: isinstance(x, property))))
        assert len(mbr_dict) == 1 and property_name == list(mbr_dict.keys())[0], \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'property_name', list(mbr_dict.keys())[0], property_name)

        subject_obj = Dummy()
        with pytest.raises(RecursionError,
                           match="Infinite recursion detected in {0}".format(property_name)):
            prop_value = subject_obj.recursion_property

        expected_counted_call = 2
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    def test_overriding_recursion_property(self):
        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'recursion_property'

        def shall_bypass_callable(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @property
            @detect_recursion(shall_bypass=shall_bypass_callable)
            def recursion_property(self):
                return self.recursion_property

        thread_local.wrapper_child_call_count = 0

        def shall_bypass_child_callable(*args, **kwargs) -> bool:
            thread_local.wrapper_child_call_count += 1
            return False  # Not to bypass recursion check

        class DummyChild(Dummy):
            @property
            @detect_recursion(shall_bypass=shall_bypass_child_callable)
            def recursion_property(self):
                return self.recursion_property

        mbr_dict = dict(inspect.getmembers(DummyChild, predicate=(lambda x: isinstance(x, property))))
        assert len(mbr_dict) == 1 and property_name == list(mbr_dict.keys())[0], \
            "ERROR in TEST: value of {0} variable needs to set right to {1}, but not to {2}.".format(
                'property_name', list(mbr_dict.keys())[0], property_name)
        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))
        assert thread_local.wrapper_child_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_child_call_count))

        subject_obj = DummyChild()
        with pytest.raises(RecursionError,
                           match="Infinite recursion detected in {0}".format(property_name)):
            prop_value = subject_obj.recursion_property
        expected_counted_call = 0
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))
        expected_counted_child_call = 2
        assert thread_local.wrapper_child_call_count == expected_counted_child_call, \
            ("Expected {0} wrapped property reference(s) in one cascade property reference, but actual {1}.".format(
                expected_counted_child_call, thread_local.wrapper_child_call_count))

        thread_local = None

    def test_class_property(self, caplog, logger_fixture: logging.Logger):  # case of decorating regular property (not with recursion logic)
        caplog.set_level(level=logging.DEBUG)

        thread_local = threading.local()
        thread_local.wrapper_call_count = 0

        property_name = 'class_property'

        def shall_bypass(*args, **kwargs) -> bool:
            thread_local.wrapper_call_count += 1
            return False  # Not to bypass recursion check

        class Dummy:
            @classmethod
            @property
            @detect_recursion(shall_bypass=shall_bypass)
            def class_property(self):
                return property_name

        assert thread_local.wrapper_call_count == 0, \
            ("ERROR in TEST: thread-local variable being used to track the number of calling wrapper already got "
             "value {0} set before actual checking.".format(thread_local.wrapper_call_count))

        assert Dummy.class_property == property_name

        expected_counted_call = 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        dummy = Dummy()
        assert dummy.class_property == property_name

        expected_counted_call += 1
        assert thread_local.wrapper_call_count == expected_counted_call, \
            ("Expected thread local value {0} as counter for wrapped static method execution, but actual {1}.".format(
                expected_counted_call, thread_local.wrapper_call_count))

        thread_local = None

    #TODO: add tests for case of class property reference of parent class from inherited class/instance.
